//
//  ImageFeedItemTableViewCell.swift
//  GrammyPlus
//
//  Created by Alina Chernenko on 5/13/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit

class ImageFeedItemTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImageView: UIImageView!
    
    @IBOutlet weak var itemTitle: UILabel!
    
    weak var dataTask: NSURLSessionDataTask?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
