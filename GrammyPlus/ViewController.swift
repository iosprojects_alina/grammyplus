//
//  ViewController.swift
//  GrammyPlus
//
//  Created by Alina Chernenko on 5/11/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit
import NXOAuth2Client

class ViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    @IBOutlet weak var refreshButton: UIButton!
    
    @IBOutlet weak var image: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.logoutButton.enabled = false
        self.refreshButton.enabled = false
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginAction(sender: AnyObject) {
        let store = NXOAuth2AccountStore.sharedStore() as! NXOAuth2AccountStore
        store.requestAccessToAccountWithType("Instagram")
        self.loginButton.enabled = false
        self.logoutButton.enabled = true
        self.refreshButton.enabled = true
        
    }

    @IBAction func logoutAction(sender: AnyObject) {
        let store = NXOAuth2AccountStore.sharedStore() as! NXOAuth2AccountStore
        for account in store.accounts as! [NXOAuth2Account] {
            if account.accountType == "Instagram" {
                store.removeAccount(account)
            }
    }
        self.loginButton.enabled = true
        self.logoutButton.enabled = false
        self.refreshButton.enabled = false
    }
    
    @IBAction func refreshAction(sender: AnyObject) {
        let instagramAccounts = NXOAuth2AccountStore.sharedStore() as! NXOAuth2AccountStore
        if (instagramAccounts.accountsWithAccountType("Instagram").count == 0){
            print("Warning: \(instagramAccounts.accountsWithAccountType("Instagram").count) accounts logged in")
            return
        }
        let account: NXOAuth2Account = instagramAccounts.accounts[0] as! NXOAuth2Account
        let token = account.accessToken.accessToken
        
        let urlString = "https://api.instagram.com/v1/users/self/media/recent/?access_token="
        let urlToken = urlString.stringByAppendingString(token)
        let URL = NSURL(string: urlToken)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(URL!, completionHandler: {
            (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            //check for network error
            if ((error) != nil) {
                print("Error, could not finish request, error is \(error)")
                return
            }
            
            //check for http error
            if let HTTPResponse = response as? NSHTTPURLResponse{
                if HTTPResponse.statusCode < 200 || HTTPResponse.statusCode >= 300 {
                    print("Error: Got status code \(HTTPResponse.statusCode)")
                    return
                }
            }
            
            //check for JSON parse error
            var parseError: NSError?
            
            do{
                
               let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                if let dataArray = json["data"] as? NSArray {
                    if let item1 = dataArray[0] as?  NSDictionary{
                        if let imagesArray = item1["images"] as? NSDictionary{
                            if let resolution = imagesArray["standard_resolution"] as? NSDictionary{
                                if let imageURL = resolution["url"] as? String{
                                    let url = NSURL(string: imageURL)
                                    let task = session.dataTaskWithURL(url!, completionHandler:{
                                        (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
                                            
                                            //check for network error
                                            if ((error) != nil) {
                                                print("Error, could not finish request")
                                                return
                                            }
                                            
                                            //check for http error
                                            if let HTTPResponse = response as? NSHTTPURLResponse{
                                                if HTTPResponse.statusCode < 200 || HTTPResponse.statusCode >= 300 {
                                                    print("Error: Got status code \(HTTPResponse.statusCode)")
                                                    return
                                                }
                                            }
                                        
                                        dispatch_async(dispatch_get_main_queue()){
                                            self.image.image = UIImage(data: data!)
                                            }

                                        }
                                        )
                                    task.resume()
                                    
                                }
                            }
                        }
                        
                    }
                }

            }
            catch {
                print("parsing error: \(parseError)")
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("raw response: \(responseString)")
                return
            }

            
        })
        task.resume()
    
    }
    

    
}

